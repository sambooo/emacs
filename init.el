(add-to-list 'load-path "~/.emacs.d/custom/")
(load "packages")
(load "global")
(load "theme")
(load "ruby")
(load "lisp")
(load "eclim")
(load "scala")
(load "php")
(load "haskell")
(load "clojure")
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-names-vector
   ["black" "red" "green" "yellow" "blue" "magenta" "cyan" "yellow"])
 '(background-color nil)
 '(background-mode dark)
 '(c-basic-offset 4)
 '(coffee-indent-tabs-mode nil)
 '(coffee-tab-width 2)
 '(cursor-color nil)
 '(custom-safe-themes
   (quote
    ("fc5fcb6f1f1c1bc01305694c59a1a861b008c534cae8d0e48e4d5e81ad718bc6" default)))
 '(foreground-color nil)
 '(global-linum-mode t)
 '(haskell-mode-hook (quote (turn-on-haskell-doc turn-on-haskell-indentation)))
 '(linum-format (quote linum-relative))
 '(linum-relative-format "%3s "))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(web-mode-html-attr-name-face ((t (:foreground "magenta"))))
 '(web-mode-html-tag-bracket-face ((t (:foreground "blue"))))
 '(web-mode-html-tag-face ((t (:foreground "blue")))))
(put 'downcase-region 'disabled nil)
