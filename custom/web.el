;; Set up coffeescript
(install-if-not-installed 'coffee-mode)
(setq coffee-tab-width 2)
