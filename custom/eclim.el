;; Eclim setup (for Java/Scala)
(install-if-not-installed 'emacs-eclim)
(require 'eclim)
(require 'eclimd)
(global-eclim-mode)

;; Setup directories
(setq eclim-eclipse-dirs "~/eclipse")
(setq eclim-executable "~/eclipse/eclim")
