;; Setup cider
(install-if-not-installed 'cider)
(install-if-not-installed 'company-cider)

(add-hook 'clojure-mode-hook 'paredit-mode)
(add-hook 'clojure-mode-hook 'rainbow-delimiters-mode)
