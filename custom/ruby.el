;; Ruby setup
(install-if-not-installed 'ruby-mode)
(install-if-not-installed 'ruby-compilation)

(install-if-not-installed 'rvm)
(require 'rvm)
(rvm-use-default)

(install-if-not-installed 'rinari)
(require 'rinari)
(global-rinari-mode)

(setq ruby-indent-level 2)

;; Setup filetypes
(add-to-list 'auto-mode-alist '("Rakefile$" . ruby-mode))
(add-to-list 'auto-mode-alist '("\\.gemspec$" . ruby-mode))
(add-to-list 'auto-mode-alist '("Gemfile$" . ruby-mode))

;; ERB Support
(install-if-not-installed 'web-mode)
(require 'web-mode)
(add-to-list 'auto-mode-alist '("\\.erb\\'" . web-mode))
(setq web-mode-extra-auto-pairs
      '(("erb" . (("open" "close")))
        ("php" . (("open" "close")
                  ("open" "close")))))
(setq web-mode-enable-auto-pairing t)

;; HAML Support
(install-if-not-installed 'haml-mode)

;; SCSS Support
(install-if-not-installed 'scss-mode)
(setq scss-compile-at-save 0)

;; Auto-add end keyword
(install-if-not-installed 'ruby-end)
(add-hook 'ruby-mode-hook 'ruby-end-mode)
