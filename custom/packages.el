;; Setup package repos
(require 'package)
(add-to-list 'package-archives
	     '("melpa" . "http://melpa.milkbox.net/packages/") t)
(when (< emacs-major-version 24)
  (add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages/")))
(package-initialize)

;; Make sure package archive is up to date
(unless package-archive-contents
  (package-refresh-contents))
(setq package-load-list '(all))

;; Install package unless it is already installed
(defun install-if-not-installed (package)
  (unless (package-installed-p package)
    (package-install package)))
