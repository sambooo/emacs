(install-if-not-installed 'color-theme-solarized)
(install-if-not-installed 'rainbow-delimiters)
(rainbow-delimiters-mode)
(load-theme 'solarized-dark t)
