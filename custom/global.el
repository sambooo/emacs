;; Global config
(install-if-not-installed 'starter-kit)
(install-if-not-installed 'ido-ubiquitous)

;; Don't show the startup screen
(setq inhibit-startup-message t)

;; Use y/n instead of yes/no
(fset 'yes-or-no-p 'y-or-n-p)

;; Highlight regions
(setq transient-mark-mode t)

;; Display line and column numbers
(setq line-number-mode t)
(setq column0number-mode t)

;; Disable beep on errors
(setq visible-bell t)

;; Make backups all go to one place
(setq backup-directory-alist '(("." . "~/.emacs.d/backups")))

;; Show matching parens
(show-paren-mode t)

;; Remove trailing whitespace from files on saving
(add-hook 'before-save-hook (lambda () (delete-trailing-whitespace)))

;; Move deleted files to trash
(setq delete-by-moving-to-trash t)

;; Install and setup smart-tab
(install-if-not-installed 'smart-tab)
(require 'smart-tab)
(global-smart-tab-mode 1)

;; Allow typing of hash-key
(defun insert-hash ()
  (interactive)
  (insert "#"))
(global-set-key (kbd "M-3") 'insert-hash)

;; Indent on newlines
(define-key global-map (kbd "RET") 'newline-and-indent)

;; Bind magit-status
(define-key global-map (kbd "C-c i") 'magit-status)

;; Enable line numbers
(install-if-not-installed 'linum-relative)
(require 'linum-relative)
(global-linum-mode)

;; Enable ido-mode
(ido-mode 1)
